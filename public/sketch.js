var circlesdict = {};
var maxrad = 80;
var decreaseby = 0.9;
var width=windowWidth;
var height=windowHeight;
var randX;
var randY;
var radius;

function preload(){
    for(i=0;i<random(1)*50 ;i++){
        circlesdict[i] = {'x':random(1)*width,
                          'y':random(1)*height,
                          'r':random(1)*maxrad};
    }
}

function setup() {
var width=windowWidth;
var height=windowHeight;    
  createCanvas(width, height);
}

function draw() {
    fill(255);
    updatecircles(circlesdict, width, height, maxrad, decreaseby);
}

function updatecircles(circlesdict, width, height, maxrad, decreaseby){
    for(var key in circlesdict){
        if (circlesdict[key]['r']< 10){
            circlesdict[key] = {'x':random(1)*width,
                      'y':random(1)*height,
                      'r':random(1)*maxrad};            

        } else {
            circlesdict[key] = {'r':decreaseby*circlesdict[key]['r']};            
        }
    }
    for (var key in circlesdict){
        stroke(255 - circlesdict[key])
        ellipse(circlesdict[key]['x'],
                circlesdict[key]['y'],
                circlesdict[key]['r'],
                circlesdict[key]['r']);
    }
}
